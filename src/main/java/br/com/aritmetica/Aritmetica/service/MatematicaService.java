package br.com.aritmetica.Aritmetica.service;

import br.com.aritmetica.Aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.Aritmetica.DTOs.RespostaDTO;
import org.springframework.stereotype.Service;

import javax.print.attribute.standard.PresentationDirection;
import java.sql.PreparedStatement;

@Service
public class MatematicaService {

    public RespostaDTO soma(EntradaDTO entradaDTO){
        int numero =0;
        RespostaDTO respostaDTO = new RespostaDTO();
        for(int n: entradaDTO.getNumeros()){
            numero += n;
        }
        respostaDTO.setResultado(numero);
        return respostaDTO;
    }

    public RespostaDTO divisao(EntradaDTO entradaDTO) {
        int resultado = entradaDTO.getNumeros().get(0);
        RespostaDTO respostaDTO = new RespostaDTO();
        for (int i = 1; i < entradaDTO.getNumeros().size(); i++) {
            resultado = resultado / entradaDTO.getNumeros().get(i);
        }
        respostaDTO.setResultado(resultado);
        return respostaDTO;
    }

    public RespostaDTO multiplicacao(EntradaDTO entradaDTO){
        int resultado = entradaDTO.getNumeros().get(0);
        RespostaDTO respostaDTO = new RespostaDTO();
        for (int i = 1; i < entradaDTO.getNumeros().size(); i++) {
            resultado = resultado*entradaDTO.getNumeros().get(i);
        }
        respostaDTO.setResultado(resultado);
        return respostaDTO;
    }

    public RespostaDTO subtracao(EntradaDTO entradaDTO){
        int resultado = entradaDTO.getNumeros().get(0);
        RespostaDTO respostaDTO = new RespostaDTO();
        for (int i = 1; i < entradaDTO.getNumeros().size(); i++) {
            resultado -= entradaDTO.getNumeros().get(i);
        }
        respostaDTO.setResultado(resultado);
        return respostaDTO;
    }
}
