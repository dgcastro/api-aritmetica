package br.com.aritmetica.Aritmetica.DTOs;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class EntradaDTO {
    List<Integer> numeros = new ArrayList<>();

    public EntradaDTO() {
    }

    public List<Integer> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Integer> numeros) {
        this.numeros = numeros;
    }

    public boolean isValid(){
        if(numeros.size()>1){
            return true;
        }else
            return false;
    }

    public boolean isValidForDivision(){
        if(numeros.size()<3 && numeros.get(0)>=numeros.get(1)) {
            return true;
        }else
            return false;
    }

    public boolean isNumerosNaturais(){
        for (int numero: numeros
             ) {
            if(numero<1){
                return false;
            }
        }
        return true;
    }
}
