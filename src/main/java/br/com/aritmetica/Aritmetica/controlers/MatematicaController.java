package br.com.aritmetica.Aritmetica.controlers;

import br.com.aritmetica.Aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.Aritmetica.DTOs.RespostaDTO;
import br.com.aritmetica.Aritmetica.service.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/matematica")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PostMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO){
        if(!entradaDTO.isValid() || !entradaDTO.isNumerosNaturais()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos 2 numeros naturais");
        }
        RespostaDTO resposta = matematicaService.soma(entradaDTO);
        return resposta;
    }

    @PostMapping("/divisao")
    public RespostaDTO divisao(@RequestBody EntradaDTO entradaDTO){
        if(!entradaDTO.isValid() || !entradaDTO.isNumerosNaturais()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos 2 numeros naturais");
        }
        if(!entradaDTO.isValidForDivision()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie exatamente dois numeros naturais e o Primeiro deve ser maior ou igual ao Segundo.");
        }
        RespostaDTO resposta= matematicaService.divisao(entradaDTO);
        return resposta;
    }

    @PostMapping("/multiplicacao")
    public RespostaDTO multiplicacao(@RequestBody EntradaDTO entradaDTO){
        if(!entradaDTO.isValid() || !entradaDTO.isNumerosNaturais()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos 2 numeros naturais");
        }
        RespostaDTO resposta = matematicaService.multiplicacao(entradaDTO);
        return resposta;
    }

    @PostMapping("/subtracao")
    public RespostaDTO subtracao(@RequestBody EntradaDTO entradaDTO){
        if(!entradaDTO.isValid() || !entradaDTO.isNumerosNaturais()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos 2 numeros naturais");
        }
        RespostaDTO respostaDTO = matematicaService.subtracao(entradaDTO);
        return respostaDTO;
    }
}